FROM node:17.2.0-buster

RUN apt-get update && apt-get install -y \
    python3 \
    python3-pip \
    build-essential \
    libssl-dev \
    libffi-dev \
    python3-dev \
 && python3 -m pip install --no-cache-dir notebook \
 && apt-get clean -y

# change existing user with uid 1000 to something else
RUN export old=$(id -un 1000) && usermod -u 9611 $old
# and also existing group
RUN export old=$(id -gn 9611) && groupmod -g 9611 $old

ARG NB_USER=jovyan
ARG NB_UID=1000

ENV USER ${NB_USER}
ENV NB_UID ${NB_UID}
ENV HOME /home/${NB_USER}

RUN adduser --disabled-password \
    --gecos "Default user" \
    --uid ${NB_UID} \
    ${NB_USER}

COPY image/ ${HOME}

RUN chown -R ${NB_UID} ${HOME}

# Too slow
# RUN chown -R ${NB_UID} /
# RUN chmod -R 777 /

RUN apt-get install -y sudo

RUN echo "$USER ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers

WORKDIR $HOME

USER ${NB_USER}

# Reset entrypoint
ENTRYPOINT []
